# JavaSeacowClient libraries

Use this in your java application to interact with the Seacow context manager.

The library is published to Maven central - https://search.maven.org/search?q=a:javaseacowclient

Use in `build.gradle` (choose a suitable version):

```
...
dependencies {
    ...
    implementation 'eu.sirenia.seacow.client:javaseacowclient:1.0.1'
}
```

or in `pom.xml`:

```
<project>
  ...
  <dependencies>
    <dependency>
      <groupId>eu.sirenia.seacow.client</groupId>
      <artifactId>javaseacowclient</artifactId>
      <version>1.0.1</version>
    </dependency>
  </dependencies>
</project>
```