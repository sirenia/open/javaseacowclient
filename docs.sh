#!/usr/bin/env bash
command -v docco >/dev/null 2>&1 || { echo >&2 "docco is missing - install with; npm install -g docco"; exit 1; }
docco -o docs src/main/java/JavaContextParticipant/*.java
